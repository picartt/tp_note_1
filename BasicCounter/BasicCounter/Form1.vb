﻿Imports BasicCounter
Public Class Form1
    Dim count = New Compteur

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_compteur.Text = count.getValue()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        count.incrementation
        lbl_compteur.Text = count.getValue()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        count.decrementation
        lbl_compteur.Text = count.getValue()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        count.remiseAZero
        lbl_compteur.Text = count.getValue()
    End Sub
End Class
