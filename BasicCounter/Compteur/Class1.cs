﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class Compteur
    {
        private int value;

        public Compteur()
        {
            this.value = 0;
        }

        public int getValue()
        {
            return this.value;
        }

        public void incrementation()
        {
            this.value++;
            return;
        }

        public void decrementation()
        {
            if (this.value == 0)
            {
                return;
            }
                
            this.value--;
            return;
        }

        public void remiseAZero()
        {
            this.value = 0;
            return;
        }
    }
}
