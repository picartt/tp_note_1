﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounter;

namespace TestUnitaireCompteur
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGetValue()
        {
            Compteur test = new Compteur();

            Assert.AreEqual(0, test.getValue());
        }

        [TestMethod]
        public void TestIncrementation()
        {
            Compteur test = new Compteur();
            test.incrementation();

            Assert.AreEqual(1, test.getValue());
        }

        [TestMethod]
        public void TestDecrementation()
        {
            Compteur test = new Compteur();
            test.incrementation();
            test.incrementation();
            test.decrementation();

            Assert.AreEqual(1, test.getValue());

            Compteur test2 = new Compteur();
            test2.decrementation();
            Assert.AreEqual(0, test2.getValue());
        }

        [TestMethod]
        public void TestRemiseAZero()
        {
            Compteur test = new Compteur();
            test.incrementation();
            test.remiseAZero();

            Assert.AreEqual(0, test.getValue());
        }
    }
}
